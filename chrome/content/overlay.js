// Copyright 2019-2025 Alessio Vanni

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:

// 1. Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following
// disclaimer in the documentation and/or other materials provided
// with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

'use strict';

var CC = Components.classes;
var CI = Components.interfaces;
var CU = Components.utils;

CU.import('resource://gre/modules/Services.jsm');

var neomelodica = {
    tabHistory: [],
    prevTabHistory: [],
    greyURI: 'chrome://neomelodica/content/grey.html',
    greyTab: null,
    closingTab: 'none',
    backGroundTabMenuItem: null,
    Strings: undefined,
    toolbarOrd: [0, 0],
    
    openLinkIn: function (url, where, params) {
	if (!where || !url)
	    return;
	
	let aFromChrome           = params.fromChrome;
	let aAllowThirdPartyFixup = params.allowThirdPartyFixup;
	let aPostData             = params.postData;
	let aCharset              = params.charset;
	let aReferrerURI          = params.referrerURI;
	let aRelatedToCurrent     = params.relatedToCurrent;
	let aAllowMixedContent    = params.allowMixedContent;
	let aInBackground         = params.inBackground;
	let aDisallowInheritPrincipal = params.disallowInheritPrincipal;
	let aInitiatingDoc        = params.initiatingDoc;
	let aIsPrivate            = params.private;
	let aSkipTabAnimation     = params.skipTabAnimation;
	let aAllowPinnedTabHostChange = !!params.allowPinnedTabHostChange;
	let aNoReferrer           = params.noReferrer;
	
	if (where == 'save') {
	    if (!aInitiatingDoc) {
		CU.reportError(neomelodica.Strings.getString('openUIError'));
		return;
	    }
	    saveURL(url, null, null, true, null, aReferrerURI, aInitiatingDoc);
	    return;
	}

	let w = getTopWin();

	if (w && !w.toolbar.visible
	    && (where == 'tab' || where == 'tabshifted'
		|| where == 'tabshiftctrl')) {
	    w = getTopWin(true);
	    aRelatedToCurrent = false;
	}
	
	if (!w || where == 'window') {
	    let sa = CC['@mozilla.org/supports-array;1']
		.createInstance(CI.nsISupportsArray);
	    
	    let wuri = CC['@mozilla.org/supports-string;1']
		.createInstance(CI.nsISupportsString);
	    
	    wuri.data = url;
	    
	    let charset = null;
	    if (aCharset) {
		charset = CC['@mozilla.org/supports-string;1']
	            .createInstance(CI.nsISupportsString);
		charset.data = 'charset=' + aCharset;
	    }
	    
	    let allowThirdPartyFixupSupports =
		CC['@mozilla.org/supports-PRBool;1']
		.createInstance(CI.nsISupportsPRBool);
	    
	    allowThirdPartyFixupSupports.data = aAllowThirdPartyFixup;
	    
	    sa.AppendElement(wuri);
	    sa.AppendElement(charset);
	    sa.AppendElement(aReferrerURI);
	    sa.AppendElement(aPostData);
	    sa.AppendElement(allowThirdPartyFixupSupports);
	    
	    let features = 'chrome,dialog=no,all';
	    if (aIsPrivate) {
		features += ',private';
	    }
	    
	    Services.ww.openWindow(w || window, getBrowserURL(),
				   null, features, sa);
	    
	    return;
	}
	
	let loadInBackground = where == 'current' ? false : aInBackground;
	if (loadInBackground == null) {
	    loadInBackground = aFromChrome ?
	        false :
	        Services.prefs.getBoolPref('browser.tabs.loadInBackground');
	}
	
	let uriObj;
	if (where == 'current') {
	    try {
		uriObj = Services.io.newURI(url, null, null);
	    } catch (e) {
		// Ignore
	    }
	}
	
	if (where == 'current' && w.gBrowser.selectedTab.pinned
	    && !aAllowPinnedTabHostChange) {
	    try {
		// nsIURI.host can throw for non-nsStandardURL nsIURIs.
		if (!uriObj
		    || (!uriObj.schemeIs('javascript')
			&& w.gBrowser.currentURI.host != uriObj.host)) {
	            where = 'tab';
	            loadInBackground = false;
		}
	    } catch (err) {
		where = 'tab';
		loadInBackground = false;
	    }
	}

	// Raise the target window before loading the URI, since
	// loading it may result in a new frontmost window
	// (e.g. 'javascript:window.open('');').
	w.focus();
	
	if (where == 'tabshiftctrl') {
	    loadInBackground = true;
	    where = 'tab';
	}
	
	switch (where) {
	case 'current':
	    let flags = CI.nsIWebNavigation.LOAD_FLAGS_NONE;
	    
	    if (aAllowThirdPartyFixup) {
		flags |= CI.nsIWebNavigation.LOAD_FLAGS_ALLOW_THIRD_PARTY_FIXUP;
		flags |= CI.nsIWebNavigation.LOAD_FLAGS_FIXUP_SCHEME_TYPOS;
	    }
	    
	    // LOAD_FLAGS_DISALLOW_INHERIT_OWNER isn't supported for
	    // javascript URIs, i.e. it causes them not to load at
	    // all. Callers should strip 'javascript:' from pasted
	    // strings to protect users from malicious URIs (see
	    // stripUnsafeProtocolOnPaste).
	    if (aDisallowInheritPrincipal
		&& !(uriObj && uriObj.schemeIs('javascript'))) {
		flags |= CI.nsIWebNavigation.LOAD_FLAGS_DISALLOW_INHERIT_OWNER;
	    }
	    
	    w.gBrowser.loadURIWithFlags(url, flags,
					aReferrerURI, null, aPostData);
	    break;
	case 'tabshifted':
	    loadInBackground = !loadInBackground;
	    // fall through
	case 'tab':
	    w.gBrowser.loadOneTab(url, {
		referrerURI: aReferrerURI,
		charset: aCharset,
		postData: aPostData,
		inBackground: loadInBackground,
		allowThirdPartyFixup: aAllowThirdPartyFixup,
		relatedToCurrent: aRelatedToCurrent,
		skipAnimation: aSkipTabAnimation,
		allowMixedContent: aAllowMixedContent,
		noReferrer: aNoReferrer
	    });
	    break;
	}
	
	w.gBrowser.selectedBrowser.focus();
	
	if (!loadInBackground && w.isBlankPageURL(url)) {
	    w.focusAndSelectUrlBar();
	}
    },
    whereToOpenLink: function (e, ignoreButton, ignoreAlt) {
	if (!e) {
	    return 'current';
	}
	
	let shift = e.shiftKey;
	let ctrl = e.ctrlKey;
	let meta = e.metaKey;
	let alt = e.altKey && !ignoreAlt;
	let middle = !ignoreButton && e.button == 1;
	let middleUsesTabs =
	    Services.prefs.getBoolPref('browser.tabs.opentabfor.middleclick',
				       true);

        if (middle || ctrl && shift) {
	    return 'tabshiftctrl';
	}
	if (ctrl) {
	    return 'tab';
	}
	if (alt) {
	    return 'save';
	}
	if (shift) {
    	    return 'tab';
	}

	if (gBrowser.tabContainer.selectedItem == neomelodica.greyTab) {
	    return 'tab';
	}
	
	return 'current';
    },
    createGreyTab: function () {
        neomelodica.greyTab = gBrowser.addTab(neomelodica.greyURI);
        neomelodica.greyTab.collapsed = 1;
        gBrowser.moveTabTo(neomelodica.greyTab, 0);
    },
    tabCloseButton: function (event) {
	if (event.target == neomelodica.greyTab) {
	    neomelodica.createGreyTab();
	}
	if (event.target == gBrowser.tabContainer.selectedItem) {
	    neomelodica.closingTab = 'same';
	} else {
	    neomelodica.closingTab = 'other';
	}
	neomelodica.updateTabHistory;
    },
    onPageLoad: function (event) {
	if (event.originalTarget.location.href == neomelodica.greyURI) {
	    if (gBrowser.getBrowserForDocument(event.originalTarget)
		!= gBrowser.getBrowserForTab(neomelodica.greyTab)) {
	        let tabs = gBrowser.tabContainer.childNodes;

	        for (let i=0; i<tabs.length; ++i) {
		    if (gBrowser.getBrowserForTab(tabs[i])
			== gBrowser.getBrowserForDocument(event.originalTarget)) {
			gBrowser.removeTab(tabs[i]);
		    }
		}
	    }
	} else if (gBrowser.getBrowserForDocument(event.originalTarget)
		   == gBrowser.getBrowserForTab(neomelodica.greyTab)) {
	    // workaround for when somehow a page is loaded in the
	    // grey tab (for example via 'show all history')
	    neomelodica.greyTab.collapsed = 0;
            gBrowser.removeEventListener('dblclick',
					 neomelodica.greyTabClicked, true);
            document.getElementById('nav-bar').collapsed = 0;
            document.getElementById('alltabs-button').collapsed = 0;
	    neomelodica.createGreyTab();
	}
    },
    init: function () {
	neomelodica.Strings = document.getElementById('strings');
	window.whereToOpenLink = neomelodica.whereToOpenLink;
	window.openLinkIn = neomelodica.openLinkIn;
	gBrowser.tabContainer.adjustTabstrip = function () {
	    switch (this.mCloseButtons) {
	    case 0:
		if (this.childNodes.length == 1
		    && this._closeWindowWithLastTab) {
		    this.setAttribute('closebuttons', 'hidden');
		} else {
		    this.setAttribute('closebuttons', 'activetab');
		}
		break;
	    case 1:
		this.setAttribute('closebuttons', 'alltabs');
		break;
	    case 2:
	    case 3:
		this.setAttribute('closebuttons', 'never');
		break;
	    default:
		break;
	    }
	    
	    let tabstripClosebutton =
		document.getElementById('tabs-closebutton');
	    
	    if (tabstripClosebutton
		&& tabstripClosebutton.parentNode == this._container) {
		tabstripClosebutton.collapsed = this.mCloseButtons != 3;
	    }
	};

	document.getElementById('appcontent')
	    .addEventListener('DOMContentLoaded', neomelodica.onPageLoad, true);

	if (document.getElementById('toolbar-menubar').scrollHeight != 0) {
	    // not a popup window
	    neomelodica.createGreyTab();
	    let tabs;
	    
            tabs = gBrowser.tabContainer.childNodes;
            for (let i=0; i<tabs.length; ++i) {
                if (tabs[i].linkedBrowser.contentDocument.location
		    == neomelodica.greyURI && tabs[i] != neomelodica.greyTab) {
                    gBrowser.removeTab(tabs[i]);
                }
            }

	    tabs = gBrowser.tabContainer.childNodes;
            for (let i=0; i<tabs.length; ++i) {
        	neomelodica.tabHistory.push(tabs[i]);
            }
	}
	
	gBrowser.tabContainer.addEventListener('TabClose',
					       neomelodica.tabCloseButton,
					       false);
        gBrowser.tabContainer.addEventListener('TabSelect',
					       neomelodica.tabSelect,
					       false);
        gBrowser.tabContainer.addEventListener('click',
					       neomelodica.tabClicked,
					       false);

        gBrowser.tabContainer._selectNewTabOrig =
	    gBrowser.tabContainer._selectNewTab;
	
        gBrowser.tabContainer._selectNewTab =
	    neomelodica._selectNewTab;

	let swapToolbar =
	    Services.prefs.getBoolPref("extensions.neomelodica.swapToolbar");

	neomelodica.toolbarOrd[0] =
	    document.getElementById('toolbar-menubar').ordinal;
	neomelodica.toolbarOrd[1] =
	    document.getElementById('PersonalToolbar').ordinal;
	
	if (swapToolbar === true) {
	    document.getElementById('toolbar-menubar').ordinal = 0;
	    document.getElementById('PersonalToolbar').ordinal = 0;
	    // document.getElementById('TabsToolbar').ordinal = 2;
	}
	
	document.getElementById('key_newNavigator')
	    .setAttribute('command', 'cmd_newNavigatorTab');

	let menuItem = document.createElement('menuitem');
	menuItem.setAttribute('id', 'neomelodica_context_closeAllTabs');
	menuItem.setAttribute('label',
			      neomelodica.Strings.getString('closeAll'));
	menuItem.setAttribute('accesskey', 'l');
	menuItem.setAttribute('oncommand', 'neomelodica.closeAll(event)');

	let ordinal = 0;
	for (let i=0; i<gBrowser.tabContextMenu.childElementCount; ++i) {
	    gBrowser.tabContextMenu.childNodes[i]
		.setAttribute('ordinal', ordinal);
	    
	    if (gBrowser.tabContextMenu.childNodes[i].id
		== 'context_closeOtherTabs') {
		gBrowser.tabContextMenu.childNodes[i]
		    .setAttribute('oncommand',
				  'neomelodica.closeOthers'
				  +'(TabContextMenu.contextTab)');
		++ordinal;
		menuItem.setAttribute('ordinal', ordinal);
	    }
	    ++ordinal;
	}

        gBrowser.tabContextMenu.appendChild(menuItem);
	
	let keyItemZ = document.createElement('key');
	keyItemZ.setAttribute('id', 'neomelodica_key_zBack');
	keyItemZ.setAttribute('key', 'Z');
	keyItemZ.setAttribute('command', 'Browser:Back');
	document.getElementById('mainKeyset').appendChild(keyItemZ);

        let keyItemX = document.createElement('key');
        keyItemX.setAttribute('id', 'neomelodica_key_xForward');
        keyItemX.setAttribute('key', 'X');
        keyItemX.setAttribute('command', 'Browser:Forward');
        document.getElementById('mainKeyset').appendChild(keyItemX);

        let keyItemPlus = document.createElement('key');
        keyItemPlus.setAttribute('id', 'neomelodica_key_plusZoomIn');
        keyItemPlus.setAttribute('key', '+');
        keyItemPlus.setAttribute('command', 'cmd_fullZoomEnlarge');
	document.getElementById('mainKeyset').appendChild(keyItemPlus);

        let keyItemMin = document.createElement('key');
        keyItemMin.setAttribute('id', 'neomelodica_key_minZoomOut');
        keyItemMin.setAttribute('key', '-');
        keyItemMin.setAttribute('command', 'cmd_fullZoomReduce');
        document.getElementById('mainKeyset').appendChild(keyItemMin);

        let keyItem6 = document.createElement('key');
        keyItem6.setAttribute('id', 'neomelodica_key_6ZoomReset');
        keyItem6.setAttribute('key', '6');
        keyItem6.setAttribute('command', 'cmd_fullZoomReset');
        document.getElementById('mainKeyset').appendChild(keyItem6);

        neomelodica.backGroundTabMenuItem = document.createElement('menuitem');
        neomelodica.backGroundTabMenuItem
	    .setAttribute('id', 'neomelodica_context_openInBackgroundTab');
        neomelodica.backGroundTabMenuItem
	    .setAttribute('label', neomelodica.Strings.getString('openBack'));
        neomelodica.backGroundTabMenuItem.setAttribute('accesskey', 'e');
        neomelodica.backGroundTabMenuItem
	    .setAttribute('oncommand',
			  'neomelodica.openInBackgroundTab(event)');

	let menu = document.getElementById('contentAreaContextMenu');
        ordinal = 0;
        for (let i=0; i < menu.childElementCount; ++i) {
            menu.childNodes[i].setAttribute('ordinal', ordinal);
            if (menu.childNodes[i].id == 'context-openlinkintab') {
                ++ordinal;
                neomelodica.backGroundTabMenuItem
		    .setAttribute('ordinal', ordinal);
            }
            ++ordinal;
        }

	menu.appendChild(neomelodica.backGroundTabMenuItem);
	menu.addEventListener('popupshowing',
			      neomelodica.openInBackgroundTabShow, false);

	Services.prefs.addObserver('extensions.neomelodica.',
				   neomelodica, false);
    },
    _selectNewTab: function (aNewTab, aFallbackDir, aWrap) {
        if (// arguments.callee.caller.name == 'onxblmousedown'
	    // &&
	    !aNewTab.selected) {
            aNewTab.setAttribute('firstclick', true);
	}
        gBrowser.tabContainer._selectNewTabOrig(aNewTab, aFallbackDir, aWrap);
    },
    updateTabHistory: function () {
	neomelodica.prevTabHistory = [];
	
	for (let i=0; i<neomelodica.tabHistory.length; ++i) {
	    neomelodica.prevTabHistory.push(neomelodica.tabHistory[i]);
	}
	
        let newTabHistory = [];
        for (let i=0; i<neomelodica.tabHistory.length; ++i) {
            let tabs = gBrowser.tabContainer.childNodes;
	    
            for (let j=0; j<tabs.length; ++j) {
                if (tabs[j] == neomelodica.tabHistory[i]) {
                    newTabHistory.push(neomelodica.tabHistory[i]);
		    break;
                }
            }
        }
        neomelodica.tabHistory = newTabHistory;
    },
    tabSelect: function (event) {
	if (gBrowser.tabContainer.selectedItem == neomelodica.greyTab) {
	    let collapse =
		Services.prefs.getBoolPref("extensions.neomelodica."
					   +"collapseToolbarOnGreyTab");
	    
	    if (collapse === true) {
		document.getElementById('nav-bar').collapsed = 1;
		document.getElementById('search-container').collapsed = 1;
		document.getElementById('alltabs-button').collapsed = 1;
	    }
	    
            gBrowser.addEventListener('dblclick',
				      neomelodica.greyTabClicked, true);
	} else {
            gBrowser.removeEventListener('dblclick',
					 neomelodica.greyTabClicked, true);
            document.getElementById('nav-bar').collapsed = 0;
            document.getElementById('search-container').collapsed = 0;
            document.getElementById('alltabs-button').collapsed = 0;
	}
        neomelodica.updateTabHistory();
        neomelodica.tabHistory.push(gBrowser.tabContainer.selectedItem);
    },
    closeAll: function () {
        let tabs = gBrowser.tabContainer.childNodes;
        for (let i=tabs.length-1; i>=0; --i) {
	    if (!gBrowser.tabContainer.childNodes[i].pinned
		&& gBrowser.tabContainer.childNodes[i] != neomelodica.greyTab) {
		gBrowser.removeTab(gBrowser.tabContainer.childNodes[i]);
	    }
        }
    },
    closeOthers: function (tab) {
        let tabs = gBrowser.tabContainer.childNodes;
        for (let i=tabs.length-1; i>=0; --i) {
            if (!gBrowser.tabContainer.childNodes[i].pinned
		&& gBrowser.tabContainer.childNodes[i] != neomelodica.greyTab
		&& tab != gBrowser.tabContainer.childNodes[i]) {
                gBrowser.removeTab(gBrowser.tabContainer.childNodes[i]);
            }
        }
    },
    greyTabClicked: function () {
        gBrowser.selectedTab = gBrowser.addTab();
        document.getElementById('urlbar').focus();
    },
    tabClicked: function (event) {
	if (event.button) {
	    // not the left button
	    return;
	}
	if (event.target.id == 'tabbrowser-tabs') {
	    // clicked on the tab bar, not on a tab
	    return;
	}
        if (event.target.hasAttribute('firstclick')) {
            event.target.removeAttribute('firstclick');
            if (!event.shiftKey) {
                return
            }
        }
        if (event.shiftKey) {
            gBrowser.removeTab(event.target);
	    neomelodica.tabHistory = [];

	    for (let i=0; i<neomelodica.prevTabHistory.length; ++i) {
		if (neomelodica.prevTabHistory[i] != event.target) {
		    neomelodica.tabHistory.push(neomelodica.prevTabHistory[i]);
		}
	    }
	    
	    if (neomelodica.tabHistory.length > 0) {
		gBrowser.tabContainer.selectedItem =
		    neomelodica.tabHistory[neomelodica.tabHistory.length-1];
	    }
	} else if (neomelodica.tabHistory.length > 1
		   && neomelodica.closingTab == 'none') {
	    neomelodica.updateTabHistory();
	    
	    let newTabIndex = neomelodica.tabHistory.length - 1;
	    while (gBrowser.tabContainer.selectedItem
		   == neomelodica.tabHistory[newTabIndex]
		   && newTabIndex > 0) {
		--newTabIndex;
	    }
	    
	    gBrowser.tabContainer.selectedItem
		= neomelodica.tabHistory[newTabIndex];
	} else if (neomelodica.closingTab != 'none') {
	    let newTabIndex = neomelodica.tabHistory.length - 3;
	    
	    if (neomelodica.closingTab == 'other') {
		newTabIndex = neomelodica.tabHistory.length - 1;
	    }
	    
	    if (newTabIndex >= 0) {
		gBrowser.tabContainer.selectedItem
		    = neomelodica.tabHistory[newTabIndex];
	    }
	    
	    neomelodica.closingTab = 'none';
	    neomelodica.updateTabHistory();
	}
    },
    openInBackgroundTab: function (event) {
	gBrowser.addTab(gContextMenu.link.toString());
    },
    openInBackgroundTabShow: function () {
	if (gContextMenu.onLink) {
	    neomelodica.backGroundTabMenuItem.collapsed = false;
	} else {
	    neomelodica.backGroundTabMenuItem.collapsed = true;
	}
    },
    // Preference observer
    observe: function (s, t, d) {
	if (t !== 'nsPref:changed') {
	    return;
	}

	if (d === 'extensions.neomelodica.swapToolbar') {
	    console.debug('ok');
	    let swapToolbar =
		Services.prefs.getBoolPref("extensions.neomelodica.swapToolbar");

	    if (swapToolbar === true) {
		document.getElementById('toolbar-menubar').ordinal = 0;
		document.getElementById('PersonalToolbar').ordinal = 0;
		// document.getElementById('TabsToolbar').ordinal = 2;
	    } else {
		document.getElementById('toolbar-menubar').ordinal =
		    neomelodica.toolbarOrd[0];
		document.getElementById('PersonalToolbar').ordinal =
		    neomelodica.toolbarOrd[1];
	    }
	}
    },
};

window.addEventListener('load', neomelodica.init, false);
